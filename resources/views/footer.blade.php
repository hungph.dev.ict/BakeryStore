<div id="footer" class="color-div">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="widget">
					<h4 class="widget-title">Hình ảnh</h4>
					<div id="beta-instagram-feed"><div></div></div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="widget">
					<h4 class="widget-title">Thông tin</h4>
					<div>
						<ul>
							<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Xây dựng</a></li>
							<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Phát triển</a></li>
							<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Quảng cáo</a></li>
{{-- 							<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Tips</a></li>
							<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Resources</a></li>
							<li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Illustrations</a></li> --}}
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
			 <div class="col-sm-10">
				<div class="widget">
					<h4 class="widget-title">Liên hệ</h4>
					<div>
						<div class="contact-info">
							<i class="fa fa-map-marker"></i>
							<p>Phạm Hoàng Hưng, Ngọc Thụy, Long Biên, Hà Nội. Điện thoại: +84 965 818 552</p>
							<p>Website cho cửa hàng bánh trực tuyến - InternProject Lifetime Technology</p>
						</div>
					</div>
				</div>
			  </div>
			</div>
			<div class="col-sm-3">
				<div class="widget">
					<h4 class="widget-title">Theo dõi</h4>
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FBamiBread%2F&tabs&width=250&height=150&small_header=true&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="250" height="150" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
			</div>
		</div> <!-- .row -->
	</div> <!-- .container -->
</div> <!-- #footer -->


