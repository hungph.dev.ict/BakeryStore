@extends('master')
@section('content')
	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Giới thiệu</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{ route('trang-chu') }}">Trang chủ</a> / <span>Giới thiệu</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content">
			<div class="our-history">
				<h2 class="text-center wow fadeInDown">Lịch sử của chúng tôi</h2>
				<div class="space35">&nbsp;</div>

				<div class="history-slider">
					<div class="history-navigation">
						<a data-slide-index="0" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2012</span></a>
						<a data-slide-index="1" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2013</span></a>
						<a data-slide-index="2" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2014</span></a>
						<a data-slide-index="3" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2015</span></a>
						<a data-slide-index="4" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2016</span></a>
						<a data-slide-index="5" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2017</span></a>
						<a data-slide-index="6" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2018</span></a>
					</div>

					<div class="history-slides">
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2012</h5>
								<p>
									Thành lập<br/>
									<br/>
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Thành lập thương hiệu</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2013</h5>
								<p>
									Xây dựng<br />
									<br />
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Xây dựng nền tảng</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2014</h5>
								<p>
									Phát triển<br />
									<br />
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Phát triển quy mô</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2015</h5>
								<p>
									Nâng cao<br />
									<br />
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Nâng cao hiệu suất</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2016</h5>
								<p>
									Mở rộng<br />
									<br />
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Mở rộng quy mô</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2017</h5>
								<p>
									Xây dựng thương hiệu<br />
									<br />
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Vững bước trên thị trường</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">2018</h5>
								<p>
									E-commerce<br />
									<br />
									
								</p>
								<div class="space20">&nbsp;</div>
								<p>Hướng tới thương mại điện tử</p>
							</div>
							</div> 
						</div>
					</div>
				</div>
			</div>

			<div class="space50">&nbsp;</div>
			<hr />
			<div class="space50">&nbsp;</div>
			<h2 class="text-center wow fadeInDown">Niềm đam mê hướng tới dịch vụ</h2>
			<div class="space20">&nbsp;</div>
			<p class="text-center wow fadeInLeft">Với đội ngũ chất lượng chúng tôi luôn đưa ra thị trường những sản phẩm hoàn hảo và tinh túy nhất.</p>
			<div class="space35">&nbsp;</div>

			<div class="row">
				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-user"></i></p>
						<p class="beta-counter-value timer numbers" data-to="19855" data-speed="2000">19855</p>
						<p class="beta-counter-title">Nhân viên</p>
					</div>
				</div>

				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-picture-o"></i></p>
						<p class="beta-counter-value timer numbers" data-to="3568" data-speed="2000">3568</p>
						<p class="beta-counter-title">Chuỗi cửa hàng</p>
					</div>
				</div>

				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-clock-o"></i></p>
						<p class="beta-counter-value timer numbers" data-to="258934" data-speed="2000">258934</p>
						<p class="beta-counter-title">Số giờ tích lũy kinh nghiệm</p>
					</div>
				</div>

				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-pencil"></i></p>
						<p class="beta-counter-value timer numbers" data-to="150" data-speed="2000">150</p>
						<p class="beta-counter-title">Sản phẩm đưa ra thị trường</p>
					</div>
				</div>
			</div> <!-- .beta-counter block end -->

			<div class="space50">&nbsp;</div>
			<hr />
			<div class="space50">&nbsp;</div>

			<h2 class="text-center wow fadeInDownwow fadeInDown">Cá nhân</h2>
			<div class="space20">&nbsp;</div>
			<h5 class="text-center other-title wow fadeInLeft">Founders</h5>
			<p class="text-center wow fadeInRight">Tiến tới đứng đầu thị trường<br />với quy mô hùng hậu.</p>
			<div class="space20">&nbsp;</div>
			<div class="row">
				<div class="col-sm-6 wow fadeInLeft">
					<div class="beta-person media">
					
						<img class="pull-left" src="source/image/person2.jpg" alt="">
					
						<div class="media-body beta-person-body">
							<h5>Phạm Hoàng Hưng</h5>
							<p class="font-large">Founder</p>
							<p>Web developer</p>
							<a href="single_type_gallery.html">View projects <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 wow fadeInRight">
					<div class="beta-person media ">
					
						<img class="pull-left" src="source/image/person3.jpg" alt="">
					
						<div class="media-body beta-person-body">
							<h5>Hoa Thị Hà</h5>
							<p class="font-large">Founder</p>
							<p>Web developer</p>
							<a href="single_type_gallery.html">View projects <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="space60">&nbsp;</div>
			<h5 class="text-center other-title wow fadeInDown">Những cá nhân xuất sắc</h5>
			<p class="text-center wow fadeInUp">Những thành tích được dựng lên<br /> từ những nỗ lực không biết mệt mỏi của tất cả mọi người</p>
			<div class="space20">&nbsp;</div>
			<div class="row">
				<div class="col-sm-3">
					<div class="beta-person beta-person-full">
				<div class="bets-img-hover">
						<img src="assets/dest/images/person1.jpg" alt="">
				</div>
						<div class="beta-person-body">
							<h5>Phạm Hoàng Hưng</h5>
							<p class="font-large">Web developer</p>
							<p>Phát triển một cộng đồng tri thức cao, đem lại hạnh phúc và sự toàn vẹn cho tất cả mọi người</p>
							<a href="single_type_gallery.html">Chi tiết <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="beta-person beta-person-full">
					<div class="bets-img-hover">
						<img src="assets/dest/images/person2.jpg" alt="">
					</div>
						<div class="beta-person-body">
							<h5>Phạm Hoàng Hưng</h5>
							<p class="font-large">Web developer</p>
							<p>Phát triển một cộng đồng tri thức cao, đem lại hạnh phúc và sự toàn vẹn cho tất cả mọi người</p>
							<a href="single_type_gallery.html">Chi tiết <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="beta-person beta-person-full">
					<div class="bets-img-hover">
						<img src="assets/dest/images/person3.jpg" alt="">
					</div>
						<div class="beta-person-body">
							<h5>Phạm Hoàng Hưng</h5>
							<p class="font-large">Web developer</p>
							<p>Phát triển một cộng đồng tri thức cao, đem lại hạnh phúc và sự toàn vẹn cho tất cả mọi người</p>
							<a href="single_type_gallery.html">Chi tiết <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="beta-person beta-person-full">
					<div class="bets-img-hover">	
						<img src="assets/dest/images/person4.jpg" alt="">
					</div>
						<div class="beta-person-body">
							<h5>Phạm Hoàng Hưng</h5>
							<p class="font-large">Web developer</p>
							<p>Phát triển một cộng đồng tri thức cao, đem lại hạnh phúc và sự toàn vẹn cho tất cả mọi người</p>
							<a href="single_type_gallery.html">Chi tiết <i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection